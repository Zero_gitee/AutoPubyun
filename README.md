# AutoPubyun

## 项目介绍
自动更新pubyun动态域名

## 实现原理
1. 利用pubyun提供的API实现更新域名IP
2. Python 3.6
3. pyinstaller（用于打包，请参考**打包.bat**）

## 业务逻辑
1. 确保网络通畅（仅限于拨号）
    1. 检查宽带是否已连接
    2. 测试www.baidu.com（可配置）
    3. 尝试拨号连接（可配置）
2. 确保各域名正确
    1. 检查域名与当前IP是否匹配
    2. 尝试更新域名（可重试）

## 使用说明

1. 在程序运行同一目录下配置**AutoPubyun.cfg**：

>{
>
>       "DomainNames": { //要更新的域名集合
>           "www.xxxx.com": "Password", //域名：密码
>           "www.xxxx.net": "Password"  //域名：密码
>       },
>       "RetryTimes": 3, //更新失败后重试的次数
>       "CheckNetworkUrl": "www.baidu.com" //用于检查网络是否通畅的Url
>       "ConnectNetworkBeforeRetry": false, //重试前是否尝试网络连接
>       "ConnectNetworkCmd": "rasdial ConnectName UserName Password", //网络连接命令
>       "LogLevel": 40 //日志级别（NOTSET = 0，DEBUG = 10，INFO = 20，WARNING = 30，ERROR = 40，CRITICAL = 50）
>}

*若无此配置文件，系统首次运行可自动生成，但仍须手动配置*

2. 运行程序即可按配置开始更新操作
3. 日志输出到logger.log
4. 建议利用Windows计划任务配置为开机运行、周期性运行，实现**自动化更新域名IP**的目的